2x Venus838 GPS providing speed data from Doppler shift measurements

MPU-6050 Gyro + Accelerometer

On arduino DUE


# Modules:

### slip_angle_sensor

main module. Arduino .ino format.

### Venus838 

Everything GPS. 
Minimal module to extract RMC NMEA message (speed, latlong, time, etc)

Measurements received are stored in gps1 and gps2.

### MPU6050

Gyroscope/accelerometer

Uses I2Cdevlib MPU6050 library https://github.com/jrowberg/i2cdevlib
