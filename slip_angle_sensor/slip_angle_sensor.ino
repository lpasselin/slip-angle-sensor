// Author: Louis-Philippe Asselin
#include "defines.h"
#include "Arduino.h"
#include "Venus838.h"
#include "MPU6050.h"

#define SKIP_MPU


Venus838 gps1 = {&Serial1, 0, 0, 0, 0, 0, 0};
Venus838 gps2 = {&Serial2, 0, 0, 0, 0, 0, 0};

#ifndef SKIP_MPU
MPU6050 ag = MPU6050();
#endif

void setup(){
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);

    #ifdef DEBUG
    Serial.begin(115200);
    debug("Escouade BMS: Slip-Angle-Sensor");
    #endif

    // init GPS and configure for 50Hz speed messages only
    gps_init(&gps1, GPS_BAUD_RATE);
    gps_init(&gps2, GPS_BAUD_RATE);
    debug("Init GPS done!");

    #ifndef SKIP_MPU
    ag.initialize();
    while(!ag.testConnection()){
        delay(100);
        debug("Waiting for MPU6050 connection...");
    }
    debug("Sensors connected");
    debug("MPU6050 Parameters : ");
    ag.setFullScaleAccelRange(MPU6050_ACCEL_FS_16);//±16g
    ag.setFullScaleGyroRange(MPU6050_GYRO_FS_2000);//±500°/s
    delay(10);
    debug(ag.getFullScaleAccelRange()); // Should be 0x03
    debug(ag.getFullScaleGyroRange()); // Should be 0x03
    debug("Offset: ");
    debug(ag.getXAccelOffset());
    debug(ag.getYAccelOffset());
    debug(ag.getZAccelOffset());
    debug(ag.getXGyroOffset());
    debug(ag.getYGyroOffset());
    debug(ag.getZGyroOffset());
    #endif

    digitalWrite(PIN_LED, HIGH);
}

void loop(){
    while(1) {
        bool new_data_gps1, new_data_gps2;
        new_data_gps1 = gps_read_speed(&gps1);
        new_data_gps2 = gps_read_speed(&gps2);

        if (new_data_gps1) {
            debug("diff ms: ");
            debug(gps1.utc_time - gps2.utc_time);
        }

        // TODO ag data proper read. Maybe use interrupt pin? See examples in I2Cdevlib


    }
}