// Author: Louis-Philippe Asselin
//
// Get 50Hz reliable speed data from Venus838 GPS
// NMEA format: https://web.archive.org/web/20171013014846/http://navspark.mybigcommerce.com/content/NMEA_Format_v0.1.pdf
// Messages Datasheet: https://web.archive.org/web/20180613225312/http://navspark.mybigcommerce.com/content/AN0028_1.4.43.pdf
// Binary messages Datasheet (raw data extension): https://web.archive.org/web/20180613225439/http://navspark.mybigcommerce.com/content/AN0030_1.4.36.pdf
//

#ifndef Venus838_h
#define Venus838_h
#include "Arduino.h"
#include "defines.h"

#define GPS_NACK 0
#define GPS_ACK 1
#define GPS_TIMEOUT 2
#define GPS_WTF 3

typedef struct {
    USARTClass* serial;
    uint32_t speed; // [10m/h] in increments of 1852: [0, 1852, 3704, 5556, ...] == [0.0 meter/h, 185.2 meter/h, 370.4 meter/h, ...]
    uint32_t course;
    int32_t latitude; // [minute/1000]
    int32_t longitude; // [minute/1000]
    int16_t msg_index; // internal msg buffer index
    byte msg_buffer[120]; // internal circular character buffer used to receive serial data
    uint32_t utc_date; // [ddmmyy]
    uint32_t utc_time;  // [hhmmsssss]
    uint32_t timestamp_speed; // [ms]
} Venus838;

#define GPS_NUM_BAUDRATES 7
// venus will not respond after 5 seconds with speed of 460k
const uint32_t GPS_BAUDRATES[GPS_NUM_BAUDRATES] = {4800, 9600, 19200, 38400, 57600, 115200, 230400};

#define GPS_CMD_

void gps_init(Venus838* gps, uint32_t baud_rate);
uint8_t gps_set_baud_rate(Venus838* gps, uint32_t baud_rate);
uint8_t gps_get_update_rate(Venus838* gps);
uint8_t gps_set_update_rate(Venus838* gps, uint8_t update_rate);
uint8_t gps_set_speed_message_only(Venus838* gps);

uint8_t gps_send_command(Venus838* gps, byte* msg, const uint8_t payload_len);

bool gps_msg_ready_to_decode(Venus838* gps);
bool gps_read_speed(Venus838* gps);

void gps_msg_set_next(Venus838* gps, byte data);
byte gps_msg_get_index(Venus838* gps, int16_t offset);

int32_t gps_parse_decimal(Venus838* gps, int16_t start_index);
int32_t gps_parse_decimal_verbose(Venus838* gps, int16_t start_index);

inline byte hex2num(byte hex){
    if (hex >= 'A' && hex <= 'F') return hex - 'A' + 10;
    else return hex - '0';
}

inline float latlong_min2deg(int32_t position){return((float)position) / 600000.0f;}

#endif