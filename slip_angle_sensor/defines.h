#ifndef SRC_DEFINES
#define SRC_DEFINES

#include "Arduino.h"

#define DEBUG 1

#define GPS_BAUD_RATE 230400
#define GPS_UPDATE_RATE 50
#define GPS_RAM_AND_FLASH 0x00
#define GPS_TIMEOUT_MS 1000

#define PIN_LED 13

#ifdef DEBUG
    #define debug(x) Serial.println(x)
#else
    #define debug(x)
#endif

#endif