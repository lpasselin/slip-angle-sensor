// Author: Louis-Philippe Asselin

#include "Venus838.h"

void gps_init(Venus838* gps, uint32_t baud_rate){
    gps_set_baud_rate(gps, baud_rate);
    gps_set_speed_message_only(gps);
    gps_set_update_rate(gps, GPS_UPDATE_RATE);
    gps_get_update_rate(gps);
}

uint8_t gps_set_baud_rate(Venus838* gps, uint32_t baud_rate){
    // check if baud rate is valid
    uint8_t baud_rate_index = 0;
    while (baud_rate_index < GPS_NUM_BAUDRATES){
        if (baud_rate == GPS_BAUDRATES[baud_rate_index]) break;
        baud_rate_index++;
    }
    if (baud_rate_index >= GPS_NUM_BAUDRATES){
        debug("CRITICAL: Baud rate is invalid");
    }

    // find baud rate by trial and error
    bool got_baud_rate = false;

    gps->serial->begin(baud_rate);
    while (got_baud_rate == false) { // loops until we get a sync
        //debug("Trying baudrate");
        gps->serial->end();
        gps->serial->begin(GPS_BAUDRATES[baud_rate_index]);
        debug((int) GPS_BAUDRATES[baud_rate_index]);
        if (gps_get_update_rate(gps) == GPS_ACK) {
            debug("synced with gps!");
            got_baud_rate = true;
            break;
        }
        baud_rate_index = (baud_rate_index+1) % GPS_NUM_BAUDRATES;

    }

    for(uint8_t i=0; i<GPS_NUM_BAUDRATES; i++){

    }
    if (got_baud_rate == false) {
        debug("CRITICAL: didn't manage to sync baud rate to GPS");
    }

    const uint8_t payload_len = 4;
    byte msg[payload_len+7];

    uint8_t p = 4;
    msg[p++] = 0x05; // cmd id
    msg[p++] = 0x00; // COM port
    msg[p++] = (byte) baud_rate_index;
    msg[p++] = GPS_RAM_AND_FLASH;

    return gps_send_command(gps, msg, payload_len);
}

uint8_t gps_get_update_rate(Venus838* gps){
    debug("Sending get update rate");
    const uint8_t payload_len = 1;
    byte msg[payload_len+7];
    uint8_t p = 4;
    msg[p++] = 0x10; // cmd id
    return gps_send_command(gps, msg, payload_len);
}

// Value in {1, 2, 4, 5, 8, 10, 20, 25, 40, 50} (does not check)
uint8_t gps_set_update_rate(Venus838* gps, uint8_t update_rate){
    debug("Sending new update rate");
    const uint8_t payload_len = 3;
    byte msg[payload_len+7];
    uint8_t p = 4;
    msg[p++] = 0x0E; // cmd id
    msg[p++] = update_rate;
    msg[p++] = GPS_RAM_AND_FLASH;
    return gps_send_command(gps, msg, payload_len);
}

uint8_t gps_set_speed_message_only(Venus838* gps){
    debug("Sending config speed nmea RMC only");
    const uint8_t payload_len = 9;
    byte msg[payload_len+7];

    uint8_t p = 4;
    msg[p++] = 0x08; // cmd id
    msg[p++] = 0x00; // GGA
    msg[p++] = 0x00; // GSA
    msg[p++] = 0x00; // GSV
    msg[p++] = 0x00; // GLL
    msg[p++] = 0x01; // RMC
    msg[p++] = 0x00; // VTG
    msg[p++] = 0x00; // ZDA
    msg[p++] = GPS_RAM_AND_FLASH;
    return gps_send_command(gps, msg, payload_len);
}

uint8_t gps_send_command(Venus838* gps, byte* msg, const uint8_t payload_len){
    msg[0] = 0xA0;
    msg[1] = 0xA1;
    msg[2] = 0x00;
    msg[3] = (byte) payload_len;
    msg[payload_len + 5] = 0x0D;
    msg[payload_len + 6] = 0x0A;

    // crc
    uint8_t checksum = msg[4];
    for(uint8_t i = 5; i< payload_len+3; i++){
        checksum ^= msg[i];
    }
    msg[payload_len + 4] = checksum;

    delay(100); // making sure the gps manages everything properly before sending new requests
    gps->serial->write(msg, payload_len+7);
    
    
    // TODO redo this piece of shit
    byte c = 0;
    byte last = 0;
    bool callback = false; // returned current cmd_id
    for(uint32_t start = millis(); millis() - start < GPS_TIMEOUT_MS;){
        while (gps->serial->available()){
            c = gps->serial->read();

            if (callback==false && last==0xA0 && c==0xA1) callback = true;

            // print update rate
            if (callback==true && last==0x86){
                debug("Update rate received is: ");
                debug((int) c);
            }

            // check for ACK
            if (callback==true && last==0x83) {
                if (c == msg[4]) return GPS_ACK; // GPS confirms he heard our input
                else return GPS_WTF;
            }
            // check for NACK
            else if (callback==true && last==0x84) {
                if (c == msg[4]) return GPS_NACK;
                else return GPS_WTF;
            }
            last = c;
        }
    }
    debug("GPS send cmd TIMEOUT");
    return GPS_TIMEOUT;
    
}

void gps_msg_set_next(Venus838* gps, byte data){
    gps->msg_buffer[gps->msg_index] = data;
    gps->msg_index = (gps->msg_index+1) % 120;
}

byte gps_msg_get_index(Venus838* gps, int16_t offset){
    offset += gps->msg_index;
    if (offset < 0) offset += 120;
    return gps->msg_buffer[offset];
}

bool gps_msg_ready_to_decode(Venus838* gps){
    // last two are CR LF
    bool last_two = (gps_msg_get_index(gps,-1)==0x0A && gps_msg_get_index(gps,-2)==0x0D);
    // has asterisk (right before the checksum)
    bool has_asterisk = gps_msg_get_index(gps, -5)=='*';
    return last_two && has_asterisk;
}

bool gps_read_speed(Venus838* gps){
    while(gps->serial->available()){
        byte new_byte = gps->serial->read();
        gps_msg_set_next(gps, new_byte);

        if (gps_msg_ready_to_decode(gps) == true){
            //gps_decode_msg_speed(gps);
            uint8_t checksum;
            checksum = hex2num(gps_msg_get_index(gps,-4)) * 16 + hex2num(gps_msg_get_index(gps,-3));

            int16_t start_offset = -6;
            while (start_offset > -121 && gps_msg_get_index(gps, start_offset) != '$'){
                checksum ^= gps_msg_get_index(gps, start_offset--);
            }
            if (checksum!=0){
                debug("Received bad gps checksum");
                return false;
            }
            else if (start_offset <= -121){ // should never happen
                debug("didn't receive a full buffer. Buffer size should be bigger than 120");
            }

            // skip $aa
            start_offset += 3;
            // check if NMEA message type is RMC
            if (gps_msg_get_index(gps, start_offset++)!='R' || 
                gps_msg_get_index(gps, start_offset++)!='M' ||
                gps_msg_get_index(gps, start_offset++)!='C')
            {
                debug("Woah gps is sending a disabled NMEA message");
                debug(gps_msg_get_index(gps, start_offset-3));
                debug(gps_msg_get_index(gps, start_offset-2));
                debug(gps_msg_get_index(gps, start_offset-1));
                return false;
            }

            gps->timestamp_speed = millis();


            start_offset++;

            // check data valid
            byte data_status = gps_msg_get_index(gps, start_offset+11);
            if (data_status!='A'){
                debug("Data invalid (are you indoor?) Data status from GPS is: ");
                debug((char) data_status);
                return false;
            }
            
            
            // UTC time in hhmmss.sss format (000000.000 ~ 235959.999) 
            uint32_t timeformatted;
            timeformatted = (uint32_t) gps_parse_decimal(gps, start_offset); // format HH'MM'SS'mmm
            // time will never be bigger than an int32 (max time is 235959999)
            uint32_t hours_to_minutes = (timeformatted / 10000000 )* 60;
            uint32_t minutes_to_seconds = (((timeformatted % 10000000) / 100000) + hours_to_minutes) * 60;
            uint32_t seconds_to_millis = ((timeformatted % 100000) / 1000 + minutes_to_seconds) * 1000;
            gps->utc_time = seconds_to_millis + timeformatted % 1000;

            // go to next value
            while(gps_msg_get_index(gps, start_offset++) != ',');
            //start_offset++;
            
            // Status already done before
            //start_offset++;
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // Latitude in dddmm.mmmm with leading zeroes
            int32_t latitude = gps_parse_decimal(gps, start_offset); // format DDD'MM'MMMM
            latitude = (latitude / 1000000)*60*10000 + (latitude % 1000000);
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // N/S indicator
            if (gps_msg_get_index(gps, start_offset) == 'S') latitude = -latitude;
            gps->latitude = latitude;
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // Longitude same as latitude
            int32_t longitude = gps_parse_decimal(gps, start_offset);
            longitude = (longitude / 1000000)*60*10000 + (longitude % 1000000);
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // E/W indicator
            if (gps_msg_get_index(gps, start_offset) == 'W') longitude = -longitude;
            gps->longitude = longitude;
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // Speed over ground (knot)
            uint32_t speed = (uint32_t) gps_parse_decimal(gps, start_offset);
            speed = speed * 1852; // speed is in 10*knot (1 knot = 1852 m/h) and increments are of 0.1knot (if we receive"00001"), which is 185.2m/h
            gps->speed = speed; // in 10*m/h
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // Course over ground
            gps->course = (uint32_t) gps_parse_decimal(gps, start_offset);
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // UTC Date
            gps->utc_date = gps_parse_decimal(gps, start_offset); // ddmmyy
            while(gps_msg_get_index(gps, start_offset++) != ',');

            // Mode indicator
            // ‘N’ = Data not valid
            // ‘A’ = Autonomous mode
            // ‘D’ = Differential mode
            // ‘E’ = Estimated (dead reckoning) mode 
            //while(gps_msg_get_index(gps, start_offset++) != ',');
            // while(gps_msg_get_index(gps, start_offset++) != ',');
            // byte mode = gps_msg_get_index(gps, start_offset)

            return true;
        }
    }
}

int32_t gps_parse_decimal(Venus838* gps, int16_t start_index){
    bool neg = (gps_msg_get_index(gps, start_index) == '-');
    if (neg) start_index++;
    int32_t ret = 0;
    while (gps_msg_get_index(gps, start_index) >= '0' && gps_msg_get_index(gps, start_index) <= '9') {
        ret = ret * 10 + gps_msg_get_index(gps, start_index++) - '0';
    }

    if (gps_msg_get_index(gps, start_index++) == '.'){
        while (gps_msg_get_index(gps, start_index) >= '0' && gps_msg_get_index(gps, start_index) <= '9'){
            ret = ret * 10 + gps_msg_get_index(gps, start_index++) - '0';
        }
    }
    return neg ? -ret : ret;
}
